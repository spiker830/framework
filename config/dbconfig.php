<?php

/* ========================================================================== *
 *
 *	Database configuration
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_USER', 'root');
define('DB_PASS', 'root');

?>
