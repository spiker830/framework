<?php

/* ========================================================================== *
 *
 * 	A wrapper around a PDO object
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class DB
{

	private static $instance;
	private static $transactionCounter = 0;

	/**
	 * 
	 * @return PDO
	 */
	public static function instance()
	{
		if (!isset(static::$instance))
		{
			$dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
			static::$instance = new PDO($dsn, DB_USER, DB_PASS);
		}

		return static::$instance;
	}

	/**
	 * Execute an arbitrary SQL query.
	 * 
	 * @param string $query The query to be executed
	 * @return PDOStatement An array, either empty or containing retrieved rows
	 */
	public static function query($query)
	{
		$statement = static::instance()->query($query, PDO::FETCH_ASSOC);
		
		if (!$statement)
		{
			Log::error(json_encode(static::instance()->errorInfo()));
			Log::error("Query attempted: $query");
		}		
		
		return $statement;
	}
	
	/**
	 * Perform a prepared SQL select query using the provided array of
	 * parameters and return an associative array of results.
	 * 
	 * @param string $query
	 * @param array $params
	 * @return array|bool An associative array of results
	 */
	public static function dbSelect($query, $params = array())
	{
		// Make sure all params are valid values
		foreach ($params as $param)
		{
			if (!DB::isSafeValue($param))
			{
				Log::error("Tried to use unsupported value: " . $param);
				return false;
			}
		}

		$statement = DB::instance()->prepare($query);
		
		$success = $statement->execute($params);
		
		// Will want to log any error
		if (!$success)
		{
			Log::error(json_encode($statement->errorInfo()));
			Log::error("Query attempted: $query");
			Log::error("with parameters: " . json_encode($params));
		}
		
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public static function dbUpdateSingle($table, $data, $id)
	{
		// Validate that id is just a plain ol' innocent id
		if (!static::isSafeValue($id))
		{
			Log::error("Tried to use unsupported id: " . $id);
			return false;
		}
		else
		{
			// Do the update
			return static::dbUpdate($table, $data, "id = " . $id);
		}
	}

	/**
	 * Update data in database using an associate array of key:value pairs to
	 * update. Each key corresponds to a column name while each value
	 * represents the field value.
	 * 
	 * @param type $sql
	 * @param type $data
	 * @return bool True on success, false on failure
	 */
	public static function dbUpdate($table, $data, $where)
	{
		$keyValues = "";
		
		if (!static::isSafeEntityName($table)) {
			Log::error("Tried to update invalid table: " . $table);
			return false;
		}
		
		// Get key/value pairs
		foreach (array_keys($data) as $key)
		{
			$keyValues .= "$key = :$key, ";
		}

		$setStr = rtrim($keyValues, ', ');

		// Build query
		// TODO Check the where clause?
		$query = "update $table set $setStr where $where";

		// Prepare
		$sth = static::instance()->prepare($query);

		// Bind values
		foreach ($data as $key => $value)
		{
			$sth->bindValue(":$key", $value);
		}
		
		$success = $sth->execute();

		// Will want to log any error
		if (!$success)
		{
			Log::error(json_encode($sth->errorInfo()));
			Log::error("Query attempted: $query");
		}

		// Return whether update succeeded
		return $success;
	}

	/**
	 * Insert data into database.
	 * 
	 * @param string $table
	 * @param array $data
	 * @return bool True on success, false on failure
	 */
	public static function dbInsert($table, $data)
	{
		// Get binding variable list (id, name)
		$bindingNames = implode("`, `", array_keys($data));
		
		// Check each key as being a safe entity name
		foreach ($data as $key => $value)
		{
			if (!static::isSafeEntityName($key))
			{
				Log::error("Attempted to use unsafe column name: " . $key);
				
				return false;
			}
		}
		
		// Get binding value list (:id, :name)
		$bindingValues = ":" . implode(", :", array_keys($data));
		
		// Build query
		$query = "insert into $table (`$bindingNames`) values ($bindingValues)";

		// Prepare
		$sth = static::instance()->prepare($query);

		// Bind values
		foreach ($data as $key => $value)
		{
			$sth->bindValue(":$key", $value);
		}
		
		// Return whether insert succeeded
		$success = $sth->execute();

		// Will want to log this error
		if (!$success)
		{
			Log::error(json_encode($sth->errorInfo()));
			Log::error("Query attempted: $query");
			Log::error("with data: " . json_encode($data));
		}

		return $success;
	}

	/**
	 * Delete a record from the table using its id.
	 * 
	 * @param string $table
	 * @param int $id
	 * @return bool True on success, false on failure
	 */
	public static function dbDelete($table, $id)
	{
		if (!static::isSafeEntityName($table)) {
			Log::error("Tried to delete from invalid table: " . $table);
			return false;
		}
		
		if (!static::isSafeValue($id)) {
			Log::error("Tried to use unsupported id: " . $id);
			return false;
		}
		
		$query = "DELETE FROM $table WHERE id = " . $id;

		$success = static::instance()->query($query);

		// Will want to log any error
		if (!$success)
		{
			Log::error(json_encode(static::instance()->errorInfo()));
			Log::error("Query: $query");
		}
		
		return $success;
	}

	/**
	 * Return a log friendly string of the last operation's error info.
	 * 
	 * @return string A JSON string of the last operation's error info
	 */
	public static function errorStr()
	{
		return json_encode(DB::instance()->errorInfo());
	}

	public static function beginTransaction()
	{
		// If we haven't already started a transaction
		if (static::$transactionCounter == 0)
		{
			// Begin transaction
			DB::instance()->beginTransaction();
		}
		
		// Increment transaction counter
		static::$transactionCounter++;
	}

	private static function commit()
	{
		// Decrement the transaction counter
		static::$transactionCounter--;
		
		// If we are at the top level transaction
		if (static::$transactionCounter == 0)
		{
			// Commit
			DB::instance()->commit();
		}
	}

	private static function rollback()
	{
		// Decrement transaction counter
		static::$transactionCounter--;
		
		// If we are at the top level transaction
		if (static::$transactionCounter == 0)
		{
			// Roll back
			DB::instance()->rollBack();
		}
	}
	
	public static function endTransaction($commit)
	{
		if ($commit)
			static::commit();
		else
			static::rollback();
	}
	
	public static function isSafeValue($value)
	{
		return preg_match("/^[a-zA-Z0-9_-]*$/", $value);
	}
	
	public static function isSafeEntityName($entityName)
	{
		return preg_match("/^[a-zA-Z_][a-zA-Z0-9_\.]*$/", $entityName);
	}
	
	public static function isSafeRelation($relation)
	{
		return in_array($relation, array("=", "!=", ">", "<", "LIKE"));
	}

}

?>
