<?php

/* ========================================================================== *
 *
 * 	A utility class used to translate between terminology use for controllers,
 * database tables, and RESTful resource names
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

abstract class Terminology
{
	private static function getSingularResourceName($resourceName)
	{
		// Get map
		$terminologyMap = unserialize(constant("TERMINOLOGY_MAP"));
		
		// Return singular resource name
		return isset($terminologyMap[$resourceName]) ? $terminologyMap[$resourceName] : null;
	}
	
	/**
	 * Get the DBObject name using a controller name. A controller name differs
	 * from a resource name in that a controller name can be a set of merged
	 * resource names (e.g. customer_contacts).
	 * 
	 * Desired results:
	 * projects -> project
	 * customer_contacts -> customerContact
	 * customer_contact_phones -> CustomerContactPhone
	 * user_levels -> userLevel
	 * 
	 * @param type $resourceName
	 * @return type
	 */
	public static function getDBObjectByControllerName($controllerName)
	{
		// Get the table name from the controller name
		$tableName = static::getTableByControllerName($controllerName);
		
		if (!isset($tableName)) return null;
		
		// Get components
		$tableNameComponents = explode("_", $tableName);
		
		// Capitalize each component
		foreach ($tableNameComponents as &$tableNameComponent) {
			
			$tableNameComponent = ucfirst($tableNameComponent);
			
		}
		
		// Strip out underscores
//		return isset($tableName) ? str_replace("_", "", $tableName) : null;
		
		return implode("", $tableNameComponents);
	}
	
	/**
	 * Get the name of the database table using the name of the controller
	 * 
	 * @param type $controllerName
	 * @return type
	 */
	private static function getTableByControllerName($controllerName)
	{
		$tableName = null;
		$controllerName = strtolower($controllerName);
		
		// Get map
		$terminologyMap = unserialize(constant("TERMINOLOGY_MAP"));
		
		// For each plural to singular mapping
		foreach ($terminologyMap as $plural => $singular)
		{
			// If the controller name ends with the plural
			if (static::endsWith($controllerName, $plural))
			{
				// Replace the key with the value in the controller name
				$tableName = preg_replace("/$plural/", $singular, $controllerName);
				
				break;
			}
		}
		
		return $tableName;
	}
	
	private static function endsWith($haystack, $needle)
	{
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}
	
	/**
	 * Get the single RESTful resource name using the names of a set of nested
	 * RESTful resource names
	 * 
	 * (e.g. ["projects", "charge_codes", "attachments"] -> "project_charge_code_attachments")
	 * 
	 * @param type $resourceNames
	 * @return type
	 */
	public static function getResourceByResourceNames($resourceNames)
	{
		// Translate all but the last resource name
		$baseResourceName = static::getTableByResourceNames(array_slice($resourceNames, 0, -1));
		
		$numElements = count($resourceNames);
		$lastResourceName = isset($resourceNames[$numElements-1]) ? $resourceNames[$numElements-1] : null;
		
		// Append last resource name
		if (isset($baseResourceName) && isset($lastResourceName))
		{
			return $baseResourceName . "_" . $lastResourceName;
		}
		else
		{
			return $lastResourceName;
		}
	}
	
	/**
	 * Get the name of the foreign key column using the RESTful resource names
	 * 
	 * @param type $resourceNames
	 * @return type
	 */
	public static function getForeignKeyByResourceNames($resourceNames)
	{
		// Translate each controller name into the table name
		$foreignKey = static::getTableByResourceNames($resourceNames);
		
		// Add the id portion
		if (!empty($foreignKey))
		{
			$foreignKey .= "_id";
		}
		
		return empty($foreignKey) ? null : $foreignKey;
	}
	
	/**
	 * Get the database table name using the RESTful resource names
	 * 
	 * @param type $resourceNames
	 * @return type
	 */
	private static function getTableByResourceNames($resourceNames)
	{
		$tableName = "";
		
		// Get singular name for each resource and concatenate with underscores
		foreach ($resourceNames as $resourceName)
		{
			if (empty($tableName))
			{
				$tableName .= static::getSingularResourceName($resourceName);
			}
			else
			{
				$tableName .= "_" . static::getSingularResourceName($resourceName);
			}
		}
		
		return empty($tableName) ? null : $tableName;
	}
	
}

?>
