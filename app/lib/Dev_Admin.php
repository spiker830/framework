<?php

/* ========================================================================== *
 *
 * 	A controller used to display a development test page
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Dev_Admin extends Controller
{
	public function index()
	{
		// Render view
		$this->view->render('../lib/dev/admin');
	}
}

?>
