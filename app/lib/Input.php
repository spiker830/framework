<?php

/* ========================================================================== *
 *
 * 	A utility class used to interact with request data
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Input
{
	/**
	 * Get the JSON in the payload as an associative array.
	 * 
	 * @return array|bool|null
	 */
	public static function json()
	{
		return json_decode(file_get_contents('php://input'), true);
	}
	
	public static function getQueryParameters() {
		
		return $_GET;
		
	}
	
}

?>