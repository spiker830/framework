<?php

/* ========================================================================== *
 *
 * 	An exception thrown during the execution of a RESTful request
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class RESTException extends Exception
{
	public function getHTTPStatusCode()
	{
		$code = parent::getCode();
		
		return $code == 0 ? HTTP::HTTP_INTERNAL_SERVER_ERROR : $code;
	}
}
?>
