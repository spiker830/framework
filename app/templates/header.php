<?php $urlBase = Configuration::getValue("URL_BASE"); ?>
<!DOCTYPE HTML>
<html>

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="">
		<meta name="keywords" content="">

		<title>Framework Skeleton</title>

		<script type="text/template" id="modal-template">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3 class="header-heading"></h3>
			<h4 class="header-subheading"></h4>
		</div>
		<div class="modal-body">
		</div>
		<div class="modal-footer">
			<div class="buttons">
			</div>
			<a href="#" class="btn done" data-dismiss="modal">Done</a>
		</div>
	</script>

	<script type="text/template" id="new-attachment-row-template">
		<tr class="new-attachment-row">
			<td colspan="10">
				<div class="form-inline">
					<input type="text" id="inputTitle" name="title" class="input-medium" placeholder="Title" />
					<!--<label for="inputFile">File: </label>-->
					<div style="width:196px;float:right">
						<button id="selectFile" class="btn btn-mini">
							<i class="icon-folder-close"></i>
							Browse
						</button>
						<button id="uploadFile" class="btn btn-mini btn-primary">
							<i class="icon-white icon-upload"></i>
							Upload
						</button>
						<button class="btn btn-mini btn-danger cancel">
							<i class="icon-white icon-remove" />
						</button>
					</div>
				</div>
				<br />
				<div id="fine-uploader"></div>
			</td>
		</tr>
	</script>

	<script type="text/template" id="list-item-attachments-button-template">
		<button class="btn btn-mini btn-primary manage-attachments">
			<i class="icon-white icon-file" />
		</button>
		(<%- numAttachments %>)
	</script>

	<script type="text/template" id="list-item-admin-buttons-template">
		<td class="edit-save-buttons">
			<button class="btn btn-mini btn-primary edit">
				<i class="icon-white icon-pencil" />
			</button>
			<button class="btn btn-mini btn-primary save">
				<i class="icon-white icon-ok" />
			</button>
		</td>
		<td class="destroy-cancel-buttons">
			<button class="btn btn-mini btn-danger destroy">
				<i class="icon-white icon-trash" />
			</button>
			<button class="btn btn-mini btn-danger cancel-edit">
				<i class="icon-white icon-remove" />
			</button>
		</td>
	</script>



	<script type="text/template" id="list-item-field-file-title-template">
		<% if (locked) { %>
		<a href="<%- attachmentURL %>">
			<%- formattedValue %>
		</a>
		<% } else { log(this); %>
		<span>
			<a href="<%- attachmentURL %>">
				<%- formattedValue %>
			</a>
		</span>
		<input type="text" value="<%- value %>" class="<%- dataId %>-value" />
		<% } %>
	</script>

	<script type="text/template" id="list-item-field-template">
		<% if (locked) { %>
		<%- formattedValue %>
		<% } else { %>
		<span>
			<%- formattedValue %>
		</span>
		<input type="text" value="<%- value %>" class="<%- dataId %>-value" />
		<% } %>
	</script>

	<script type="text/javascript">
		var urlBase = "<?php echo $urlBase ?>";
	</script>

	<script src="<?php echo $urlBase ?>app/js/lib/require.js"></script>
	
	<script type="text/javascript">
		require.config({
	
			baseUrl: '<?php echo $urlBase ?>app/js/lib',
			paths: {
				app: '../app',
				fileUploader: '../fileUploader'
			},
			shim: {
				bootstrap: {
					deps: ['jquery']
				},
				underscore: {
					exports: '_'
				},
				backbone: {
					deps: [
						'underscore',
						'jquery'
					],
					exports: 'Backbone'
				},
				jquery: {
					exports: 'jQuery'
				},
				'backbone-util': ['backbone'],
				'jquery-ui.custom.min': ['jquery']
//				'util': ['jquery'],
			}
		});
		
		require(['util', 'jquery-ui.custom.min', 'app/<?php echo $this->name ?>']);
		
	</script>
	
<!--	<script src="<?php echo $urlBase ?>app/js/app/<?php echo $this->name ?>.js"></script>-->


	<?php foreach ($this->css as $css): ?>
		<link rel="stylesheet" href="<?php echo $urlBase ?>app/css/<?php echo $css ?>.css"></link>
	<?php endforeach; ?>

	<?php foreach ($this->js as $js): ?>
		<script type="text/javascript" src="<?php echo $urlBase ?>app/js/<?php echo $js ?>.js"></script>
	<?php endforeach; ?>

	<script type="text/javascript">
		define('mega', ['jquery'],
	function($) {
		$(window).load(function() {

			$("li").hover(addMega, removeMega);

		});

		function addMega(){
			$(this).addClass("hovering");
		}

		function removeMega(){
			$(this).removeClass("hovering");
		}
	});
	</script>

</head>

<body>
	<div class="container">

		<div class="header">
		</div>




		<ul class="nav shadow">

			<?php if (Session::get('loggedIn')): ?>
				<li class="button"><a href="<?php echo $urlBase ?>logout">Logout</a></li>
			<?php else: ?>
				<li class="button"><a href="<?php echo $urlBase ?>login">Login</a></li>
			<?php endif; ?>

			<span class="greeting pull-right">
				<?php if (Session::get('loggedIn')): ?>
					<?php echo Configuration::getValue("GREETING_TEXT") . ' ' . ucfirst(Session::get('employeeName')) ?>
				<?php endif; ?>
			</span>
		</ul>

		<div class="content shadow">