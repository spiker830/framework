<?php

/* ========================================================================== *
 *
 *	RESTful resource controller for users/employees.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Employees extends RESTfulController
{
	protected static $viewPrivilege = "view employee page";

}

?>
