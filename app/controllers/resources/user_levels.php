<?php

/* ========================================================================== *
 *
 * 	RESTful resource controller for user levels.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class User_Levels extends RESTfulController
{
	static $viewPrivilege = "view privileges";
	static $editPrivilege = "edit privileges";
	
}

?>
