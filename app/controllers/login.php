<?php

/* ========================================================================== *
 *
 * 	The login controller which displays the login page as well as handles the
 * login process.
 *
 * 	@author: Jonathan Spiker
 *
 * ========================================================================== */

class Login extends Controller
{

	protected static $public = true;

	function index()
	{
		$this->view->render('login');
	}

	/**
	 * Attempts to log a user into the application. Sets session parameters if
	 * successful, redirects to default page otherwise.
	 */
	function run()
	{
		Session::init();

		$statement = $this->model->run();

		if ($statement->rowCount() > 0)
		{
			Session::set('loggedIn', true);

			$row = $statement->fetch(PDO::FETCH_ASSOC);
			$employeeId = $row['id'];

			Session::set('employeeId', $employeeId);

			Session::set('level', $this->model->getLevel($employeeId));
			Session::set('levelName', $this->model->getLevelName($employeeId));
			Session::set('employeeName', $this->model->getEmployeeName($employeeId));
		}
		else
		{
			Session::set('loggedIn', false);
		}

		header('Location: ' . Configuration::getValue("URL_BASE") . Configuration::getValue("DEFAULT_PAGE"));
	}

}

?>
