# Framework Skeleton #

This is my PHP web application framework I have worked on for the last couple years in my spare time that I choose to devote to software. I use this as my test bed for learning new techniques and technologies.

## Disclaimer ##

Since I use this framework as my test bed, there are plenty of cases where I have changed my mind and left part of the framework lagging.

Things on my immediate to-do list

* app/js/lib/backbone-util.js is not broken up yet as I have not completed my transition to using RequireJS
* app/models represents my former database abstraction layer and is being replaced by my custom ORM
* database access is tightly coupled with my ORM (i.e. It only works with my database abstraction class, DB)
* build.xml is a placeholder for future implementation